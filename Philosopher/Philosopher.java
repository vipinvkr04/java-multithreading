import java.util.Random;

public class Philosopher implements Runnable{
	private int id;
	private ChopStick leftChopStick;
	private ChopStick rightChopStick;
	private volatile boolean isFull = false;
	private int eatingCounter;
	
	public Philosopher(int id, ChopStick leftChopStick, ChopStick rightChopStick) {
		this.id = id;
		this.leftChopStick = leftChopStick;
		this.rightChopStick = rightChopStick;
	}

	@Override
	public void run() {
		try {
		while(!isFull) {		
			think();
			if(leftChopStick.pickUp(this, State.LEFT)) {
				if(rightChopStick.pickUp(this, State.RIGHT)) {
					eat();
					rightChopStick.putDown(this, State.RIGHT);
				}
				leftChopStick.putDown(this, State.LEFT);
			}
		 }
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void think() throws InterruptedException {
		System.out.println(this+" is thinking...");
		Thread.sleep(200);
	}
	
	private void eat() throws InterruptedException {
		System.out.println(this+" is eating...");
		this.eatingCounter++;
		Thread.sleep(200);
	}
	
	public int getEatingCounter(){
		return this.eatingCounter;
	}
	
	public void setFull(boolean isFull){
		this.isFull = isFull;
	}
	
	@Override
	public String toString() {
		return "Philosopher-"+this.id;
	}
}
